var spawn = require('child_process').spawnSync;
var curlParser = require('curl-trace-parser');
var httpParser = require('http-string-parser');
var bf = require('api-blueprint-http-formatter')

function run(params){
    try{
        params.shift();
        params.shift();
        var curlParams = [...params, "--trace", "-"];
        obj = spawn("curl", curlParams);
        if(obj.error){
            console.log(obj.error);
            return 1;
        }

        if(obj.status !== 0){
            var stderr = String(obj.stderr);
            if(stderr.length > 0){
                process.stdout.write(stderr);
            }
            return obj.status;
        }

        stdout = String(obj.stdout);

        var rawPair = curlParser.parse(stdout);
        parsedPair = {
          'request': httpParser.parseRequest(rawPair['request']),
          'response': httpParser.parseResponse(rawPair['response'])
        }
        var blueprint = bf.format(parsedPair);
        process.stdout.write(blueprint);

        return obj.status;
    }
    catch(e){
        return 1;
    }
};

module.exports = {
    run
};