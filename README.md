# BluePrint cURL

Works like cURL but displays output in API Blueprint format

## Requirements

cURL needs to be installed

## Usage

```
$ bluecurl http://httpbin.org/ip
```

## Install the BluePrint cURL

```bash
$ npm install -g bluecurl
```